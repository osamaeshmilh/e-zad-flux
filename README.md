# FluxStore Product

# 1. Clean Architecture
- Bloc 

# 2. Development
- Flutter version 1.12.13+hotfix.6

##  Provider v4.0.1

Obtains the nearest [Provider<T>] up its widget tree and returns its value.

If [listen] is true, later value changes will trigger a new [State.build] to widgets, and [State.didChangeDependencies] for [StatefulWidget].

By default, listen is inferred based on wether the widget tree is currently building or not:

if widgets are building, listen is true
if widgets aren't, listen is false.

https://pub.dev/packages/provider#-changelog-tab-
https://github.com/rrousselGit/provider/issues/305
https://github.com/rrousselGit/provider/blob/master/packages/provider/example/lib/main.dart

## Firebase HashKey 
```
    keytool -list -printcert -jarfile build/app/outputs/apk/release/app-release.apk
```

## Submit Store
  Android build bundle optimize apk size: build/app/outputs/bundle/release/app-release.aab
  ```
  flutter build appbundle
  ```

## Run mock data local - remove dependency on API (no integration with the backend site)

- Open file `lib/common/config.dart`, find variable `serverConfig` and change from :

```
const serverConfig = serverConfigReal;
```

to 

```
const serverConfig = serverConfigMock;
```

# 3. App INFO 

## 3.1. MStore Flutter 
BundldID
  android: applicationId 
  ```
  com.inspireui.fluxstore
  ```
  iOS: PRODUCT_BUNDLE_IDENTIFIER
  ```
  com.inspireui.mstore.flutter
  ```
Store live: https://play.google.com/store/apps/details?id=com.inspireui.fluxstore
beta testing: https://play.google.com/apps/testing/com.inspireui.fluxstore

## 3.2. Tutorial build appbundle android

- Step 1: up version code in file pubspec.yaml

Example

```
  version: 1.6.3+27
```

to 

```
  version: 1.6.3+28
```

- Step 2: run script build 

```
  bash run_build_appbundle.sh
```

