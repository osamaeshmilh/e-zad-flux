// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `See All`
  String get seeAll {
    return Intl.message(
      'See All',
      name: 'seeAll',
      desc: '',
      args: [],
    );
  }

  /// `Feature Products`
  String get featureProducts {
    return Intl.message(
      'Feature Products',
      name: 'featureProducts',
      desc: '',
      args: [],
    );
  }

  /// `Gears Collections`
  String get bagsCollections {
    return Intl.message(
      'Gears Collections',
      name: 'bagsCollections',
      desc: '',
      args: [],
    );
  }

  /// `Woman Collections`
  String get womanCollections {
    return Intl.message(
      'Woman Collections',
      name: 'womanCollections',
      desc: '',
      args: [],
    );
  }

  /// `Man Collections`
  String get manCollections {
    return Intl.message(
      'Man Collections',
      name: 'manCollections',
      desc: '',
      args: [],
    );
  }

  /// `Buy Now`
  String get buyNow {
    return Intl.message(
      'Buy Now',
      name: 'buyNow',
      desc: '',
      args: [],
    );
  }

  /// `Products`
  String get products {
    return Intl.message(
      'Products',
      name: 'products',
      desc: '',
      args: [],
    );
  }

  /// `Add To Cart`
  String get addToCart {
    return Intl.message(
      'Add To Cart',
      name: 'addToCart',
      desc: '',
      args: [],
    );
  }

  /// `Description`
  String get description {
    return Intl.message(
      'Description',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Reviews`
  String get readReviews {
    return Intl.message(
      'Reviews',
      name: 'readReviews',
      desc: '',
      args: [],
    );
  }

  /// `Additional Information`
  String get additionalInformation {
    return Intl.message(
      'Additional Information',
      name: 'additionalInformation',
      desc: '',
      args: [],
    );
  }

  /// `No Reviews`
  String get noReviews {
    return Intl.message(
      'No Reviews',
      name: 'noReviews',
      desc: '',
      args: [],
    );
  }

  /// `The product is added`
  String get productAdded {
    return Intl.message(
      'The product is added',
      name: 'productAdded',
      desc: '',
      args: [],
    );
  }

  /// `You might also like`
  String get youMightAlsoLike {
    return Intl.message(
      'You might also like',
      name: 'youMightAlsoLike',
      desc: '',
      args: [],
    );
  }

  /// `Select the size`
  String get selectTheSize {
    return Intl.message(
      'Select the size',
      name: 'selectTheSize',
      desc: '',
      args: [],
    );
  }

  /// `Select the color`
  String get selectTheColor {
    return Intl.message(
      'Select the color',
      name: 'selectTheColor',
      desc: '',
      args: [],
    );
  }

  /// `Select the quantity`
  String get selectTheQuantity {
    return Intl.message(
      'Select the quantity',
      name: 'selectTheQuantity',
      desc: '',
      args: [],
    );
  }

  /// `Size`
  String get size {
    return Intl.message(
      'Size',
      name: 'size',
      desc: '',
      args: [],
    );
  }

  /// `Color`
  String get color {
    return Intl.message(
      'Color',
      name: 'color',
      desc: '',
      args: [],
    );
  }

  /// `My Cart`
  String get myCart {
    return Intl.message(
      'My Cart',
      name: 'myCart',
      desc: '',
      args: [],
    );
  }

  /// `Save to Wishlist`
  String get saveToWishList {
    return Intl.message(
      'Save to Wishlist',
      name: 'saveToWishList',
      desc: '',
      args: [],
    );
  }

  /// `Share`
  String get share {
    return Intl.message(
      'Share',
      name: 'share',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Checkout`
  String get checkout {
    return Intl.message(
      'Checkout',
      name: 'checkout',
      desc: '',
      args: [],
    );
  }

  /// `Clear Cart`
  String get clearCart {
    return Intl.message(
      'Clear Cart',
      name: 'clearCart',
      desc: '',
      args: [],
    );
  }

  /// `My Wishlist`
  String get myWishList {
    return Intl.message(
      'My Wishlist',
      name: 'myWishList',
      desc: '',
      args: [],
    );
  }

  /// `Your bag is empty`
  String get yourBagIsEmpty {
    return Intl.message(
      'Your bag is empty',
      name: 'yourBagIsEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Looks like you haven’t added any items to the bag yet. Start shopping to fill it in.`
  String get emptyCartSubtitle {
    return Intl.message(
      'Looks like you haven’t added any items to the bag yet. Start shopping to fill it in.',
      name: 'emptyCartSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Start Shopping`
  String get startShopping {
    return Intl.message(
      'Start Shopping',
      name: 'startShopping',
      desc: '',
      args: [],
    );
  }

  /// `No favourites yet.`
  String get noFavoritesYet {
    return Intl.message(
      'No favourites yet.',
      name: 'noFavoritesYet',
      desc: '',
      args: [],
    );
  }

  /// `Tap any heart next to a product to favotite. We’ll save them for you here!`
  String get emptyWishlistSubtitle {
    return Intl.message(
      'Tap any heart next to a product to favotite. We’ll save them for you here!',
      name: 'emptyWishlistSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Search for Items`
  String get searchForItems {
    return Intl.message(
      'Search for Items',
      name: 'searchForItems',
      desc: '',
      args: [],
    );
  }

  /// `Shipping`
  String get shipping {
    return Intl.message(
      'Shipping',
      name: 'shipping',
      desc: '',
      args: [],
    );
  }

  /// `review`
  String get review {
    return Intl.message(
      'review',
      name: 'review',
      desc: '',
      args: [],
    );
  }

  /// `Payment`
  String get payment {
    return Intl.message(
      'Payment',
      name: 'payment',
      desc: '',
      args: [],
    );
  }

  /// `First Name`
  String get firstName {
    return Intl.message(
      'First Name',
      name: 'firstName',
      desc: '',
      args: [],
    );
  }

  /// `Last Name`
  String get lastName {
    return Intl.message(
      'Last Name',
      name: 'lastName',
      desc: '',
      args: [],
    );
  }

  /// `City`
  String get city {
    return Intl.message(
      'City',
      name: 'city',
      desc: '',
      args: [],
    );
  }

  /// `State / Province`
  String get stateProvince {
    return Intl.message(
      'State / Province',
      name: 'stateProvince',
      desc: '',
      args: [],
    );
  }

  /// `Zip-code`
  String get zipCode {
    return Intl.message(
      'Zip-code',
      name: 'zipCode',
      desc: '',
      args: [],
    );
  }

  /// `Country`
  String get country {
    return Intl.message(
      'Country',
      name: 'country',
      desc: '',
      args: [],
    );
  }

  /// `Phone number`
  String get phoneNumber {
    return Intl.message(
      'Phone number',
      name: 'phoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Street Name`
  String get streetName {
    return Intl.message(
      'Street Name',
      name: 'streetName',
      desc: '',
      args: [],
    );
  }

  /// `Shipping Method`
  String get shippingMethod {
    return Intl.message(
      'Shipping Method',
      name: 'shippingMethod',
      desc: '',
      args: [],
    );
  }

  /// `Continue to Shipping`
  String get continueToShipping {
    return Intl.message(
      'Continue to Shipping',
      name: 'continueToShipping',
      desc: '',
      args: [],
    );
  }

  /// `Continue to Review`
  String get continueToReview {
    return Intl.message(
      'Continue to Review',
      name: 'continueToReview',
      desc: '',
      args: [],
    );
  }

  /// `Continue to Payment`
  String get continueToPayment {
    return Intl.message(
      'Continue to Payment',
      name: 'continueToPayment',
      desc: '',
      args: [],
    );
  }

  /// `Go back to address`
  String get goBackToAddress {
    return Intl.message(
      'Go back to address',
      name: 'goBackToAddress',
      desc: '',
      args: [],
    );
  }

  /// `Go back to shipping`
  String get goBackToShipping {
    return Intl.message(
      'Go back to shipping',
      name: 'goBackToShipping',
      desc: '',
      args: [],
    );
  }

  /// `Go back to review`
  String get goBackToReview {
    return Intl.message(
      'Go back to review',
      name: 'goBackToReview',
      desc: '',
      args: [],
    );
  }

  /// `Address`
  String get address {
    return Intl.message(
      'Address',
      name: 'address',
      desc: '',
      args: [],
    );
  }

  /// `Shipping Address`
  String get shippingAddress {
    return Intl.message(
      'Shipping Address',
      name: 'shippingAddress',
      desc: '',
      args: [],
    );
  }

  /// `Order details`
  String get orderDetail {
    return Intl.message(
      'Order details',
      name: 'orderDetail',
      desc: '',
      args: [],
    );
  }

  /// `Subtotal`
  String get subtotal {
    return Intl.message(
      'Subtotal',
      name: 'subtotal',
      desc: '',
      args: [],
    );
  }

  /// `Total`
  String get total {
    return Intl.message(
      'Total',
      name: 'total',
      desc: '',
      args: [],
    );
  }

  /// `Payment Methods`
  String get paymentMethods {
    return Intl.message(
      'Payment Methods',
      name: 'paymentMethods',
      desc: '',
      args: [],
    );
  }

  /// `Choose your payment method`
  String get chooseYourPaymentMethod {
    return Intl.message(
      'Choose your payment method',
      name: 'chooseYourPaymentMethod',
      desc: '',
      args: [],
    );
  }

  /// `Place My Order`
  String get placeMyOrder {
    return Intl.message(
      'Place My Order',
      name: 'placeMyOrder',
      desc: '',
      args: [],
    );
  }

  /// `It's ordered!`
  String get itsOrdered {
    return Intl.message(
      'It\'s ordered!',
      name: 'itsOrdered',
      desc: '',
      args: [],
    );
  }

  /// `Order No.`
  String get orderNo {
    return Intl.message(
      'Order No.',
      name: 'orderNo',
      desc: '',
      args: [],
    );
  }

  /// `Show All My Ordered`
  String get showAllMyOrdered {
    return Intl.message(
      'Show All My Ordered',
      name: 'showAllMyOrdered',
      desc: '',
      args: [],
    );
  }

  /// `Back to Shop`
  String get backToShop {
    return Intl.message(
      'Back to Shop',
      name: 'backToShop',
      desc: '',
      args: [],
    );
  }

  /// `The first name field is required`
  String get firstNameIsRequired {
    return Intl.message(
      'The first name field is required',
      name: 'firstNameIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `The last name field is required`
  String get lastNameIsRequired {
    return Intl.message(
      'The last name field is required',
      name: 'lastNameIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `The street name field is required`
  String get streetIsRequired {
    return Intl.message(
      'The street name field is required',
      name: 'streetIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `The city field is required`
  String get cityIsRequired {
    return Intl.message(
      'The city field is required',
      name: 'cityIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `The state field is required`
  String get stateIsRequired {
    return Intl.message(
      'The state field is required',
      name: 'stateIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `The country field is required`
  String get countryIsRequired {
    return Intl.message(
      'The country field is required',
      name: 'countryIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `The phone number field is required`
  String get phoneIsRequired {
    return Intl.message(
      'The phone number field is required',
      name: 'phoneIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `The email field is required`
  String get emailIsRequired {
    return Intl.message(
      'The email field is required',
      name: 'emailIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `The zip code field is required`
  String get zipCodeIsRequired {
    return Intl.message(
      'The zip code field is required',
      name: 'zipCodeIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `No Orders`
  String get noOrders {
    return Intl.message(
      'No Orders',
      name: 'noOrders',
      desc: '',
      args: [],
    );
  }

  /// `Order Date`
  String get orderDate {
    return Intl.message(
      'Order Date',
      name: 'orderDate',
      desc: '',
      args: [],
    );
  }

  /// `Status`
  String get status {
    return Intl.message(
      'Status',
      name: 'status',
      desc: '',
      args: [],
    );
  }

  /// `Payment Method`
  String get paymentMethod {
    return Intl.message(
      'Payment Method',
      name: 'paymentMethod',
      desc: '',
      args: [],
    );
  }

  /// `Order History`
  String get orderHistory {
    return Intl.message(
      'Order History',
      name: 'orderHistory',
      desc: '',
      args: [],
    );
  }

  /// `Refund Request`
  String get refundRequest {
    return Intl.message(
      'Refund Request',
      name: 'refundRequest',
      desc: '',
      args: [],
    );
  }

  /// `History`
  String get recentSearches {
    return Intl.message(
      'History',
      name: 'recentSearches',
      desc: '',
      args: [],
    );
  }

  /// `Recent`
  String get recents {
    return Intl.message(
      'Recent',
      name: 'recents',
      desc: '',
      args: [],
    );
  }

  /// `By Price`
  String get byPrice {
    return Intl.message(
      'By Price',
      name: 'byPrice',
      desc: '',
      args: [],
    );
  }

  /// `By Category`
  String get byCategory {
    return Intl.message(
      'By Category',
      name: 'byCategory',
      desc: '',
      args: [],
    );
  }

  /// `No internet connection`
  String get noInternetConnection {
    return Intl.message(
      'No internet connection',
      name: 'noInternetConnection',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `General Setting`
  String get generalSetting {
    return Intl.message(
      'General Setting',
      name: 'generalSetting',
      desc: '',
      args: [],
    );
  }

  /// `Get Notification`
  String get getNotification {
    return Intl.message(
      'Get Notification',
      name: 'getNotification',
      desc: '',
      args: [],
    );
  }

  /// `Notify Messages`
  String get listMessages {
    return Intl.message(
      'Notify Messages',
      name: 'listMessages',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get language {
    return Intl.message(
      'Language',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Dark Theme`
  String get darkTheme {
    return Intl.message(
      'Dark Theme',
      name: 'darkTheme',
      desc: '',
      args: [],
    );
  }

  /// `Rate the app`
  String get rateTheApp {
    return Intl.message(
      'Rate the app',
      name: 'rateTheApp',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get logout {
    return Intl.message(
      'Logout',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `LogIn`
  String get login {
    return Intl.message(
      'LogIn',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `items`
  String get items {
    return Intl.message(
      'items',
      name: 'items',
      desc: '',
      args: [],
    );
  }

  /// `Cart`
  String get cart {
    return Intl.message(
      'Cart',
      name: 'cart',
      desc: '',
      args: [],
    );
  }

  /// `Shop`
  String get shop {
    return Intl.message(
      'Shop',
      name: 'shop',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Blog`
  String get blog {
    return Intl.message(
      'Blog',
      name: 'blog',
      desc: '',
      args: [],
    );
  }

  /// `Apply`
  String get apply {
    return Intl.message(
      'Apply',
      name: 'apply',
      desc: '',
      args: [],
    );
  }

  /// `Reset`
  String get reset {
    return Intl.message(
      'Reset',
      name: 'reset',
      desc: '',
      args: [],
    );
  }

  /// `Sign in with email`
  String get signInWithEmail {
    return Intl.message(
      'Sign in with email',
      name: 'signInWithEmail',
      desc: '',
      args: [],
    );
  }

  /// `Don't have an account?`
  String get dontHaveAccount {
    return Intl.message(
      'Don\'t have an account?',
      name: 'dontHaveAccount',
      desc: '',
      args: [],
    );
  }

  /// `Sign up`
  String get signup {
    return Intl.message(
      'Sign up',
      name: 'signup',
      desc: '',
      args: [],
    );
  }

  /// `Welcome`
  String get welcome {
    return Intl.message(
      'Welcome',
      name: 'welcome',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get close {
    return Intl.message(
      'Close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `OR`
  String get or {
    return Intl.message(
      'OR',
      name: 'or',
      desc: '',
      args: [],
    );
  }

  /// `Please input fill in all fields`
  String get pleaseInput {
    return Intl.message(
      'Please input fill in all fields',
      name: 'pleaseInput',
      desc: '',
      args: [],
    );
  }

  /// `Searching Address`
  String get searchingAddress {
    return Intl.message(
      'Searching Address',
      name: 'searchingAddress',
      desc: '',
      args: [],
    );
  }

  /// `Out of stock`
  String get outOfStock {
    return Intl.message(
      'Out of stock',
      name: 'outOfStock',
      desc: '',
      args: [],
    );
  }

  /// `Unavailable`
  String get unavailable {
    return Intl.message(
      'Unavailable',
      name: 'unavailable',
      desc: '',
      args: [],
    );
  }

  /// `Category`
  String get category {
    return Intl.message(
      'Category',
      name: 'category',
      desc: '',
      args: [],
    );
  }

  /// `No Product`
  String get noProduct {
    return Intl.message(
      'No Product',
      name: 'noProduct',
      desc: '',
      args: [],
    );
  }

  /// `We found {length} products`
  String weFoundProducts(Object length) {
    return Intl.message(
      'We found $length products',
      name: 'weFoundProducts',
      desc: '',
      args: [length],
    );
  }

  /// `Clear`
  String get clear {
    return Intl.message(
      'Clear',
      name: 'clear',
      desc: '',
      args: [],
    );
  }

  /// `Video`
  String get video {
    return Intl.message(
      'Video',
      name: 'video',
      desc: '',
      args: [],
    );
  }

  /// `Your Recent View`
  String get recentView {
    return Intl.message(
      'Your Recent View',
      name: 'recentView',
      desc: '',
      args: [],
    );
  }

  /// `In stock`
  String get inStock {
    return Intl.message(
      'In stock',
      name: 'inStock',
      desc: '',
      args: [],
    );
  }

  /// `Tracking number is`
  String get trackingNumberIs {
    return Intl.message(
      'Tracking number is',
      name: 'trackingNumberIs',
      desc: '',
      args: [],
    );
  }

  /// `Availability`
  String get availability {
    return Intl.message(
      'Availability',
      name: 'availability',
      desc: '',
      args: [],
    );
  }

  /// `Tracking page`
  String get trackingPage {
    return Intl.message(
      'Tracking page',
      name: 'trackingPage',
      desc: '',
      args: [],
    );
  }

  /// `My points`
  String get myPoints {
    return Intl.message(
      'My points',
      name: 'myPoints',
      desc: '',
      args: [],
    );
  }

  /// `You have $point points`
  String get youHavePoints {
    return Intl.message(
      'You have \$point points',
      name: 'youHavePoints',
      desc: '',
      args: [],
    );
  }

  /// `Events`
  String get events {
    return Intl.message(
      'Events',
      name: 'events',
      desc: '',
      args: [],
    );
  }

  /// `Date`
  String get date {
    return Intl.message(
      'Date',
      name: 'date',
      desc: '',
      args: [],
    );
  }

  /// `Point`
  String get point {
    return Intl.message(
      'Point',
      name: 'point',
      desc: '',
      args: [],
    );
  }

  /// `Order notes`
  String get orderNotes {
    return Intl.message(
      'Order notes',
      name: 'orderNotes',
      desc: '',
      args: [],
    );
  }

  /// `Please rating before you send your comment`
  String get ratingFirst {
    return Intl.message(
      'Please rating before you send your comment',
      name: 'ratingFirst',
      desc: '',
      args: [],
    );
  }

  /// `Please write your comment`
  String get commentFirst {
    return Intl.message(
      'Please write your comment',
      name: 'commentFirst',
      desc: '',
      args: [],
    );
  }

  /// `Write your comment`
  String get writeComment {
    return Intl.message(
      'Write your comment',
      name: 'writeComment',
      desc: '',
      args: [],
    );
  }

  /// `Loading...`
  String get loading {
    return Intl.message(
      'Loading...',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `Your rating`
  String get productRating {
    return Intl.message(
      'Your rating',
      name: 'productRating',
      desc: '',
      args: [],
    );
  }

  /// `Layouts`
  String get layout {
    return Intl.message(
      'Layouts',
      name: 'layout',
      desc: '',
      args: [],
    );
  }

  /// `Select Address`
  String get selectAddress {
    return Intl.message(
      'Select Address',
      name: 'selectAddress',
      desc: '',
      args: [],
    );
  }

  /// `Save Address`
  String get saveAddress {
    return Intl.message(
      'Save Address',
      name: 'saveAddress',
      desc: '',
      args: [],
    );
  }

  /// `Please write input in search field`
  String get searchInput {
    return Intl.message(
      'Please write input in search field',
      name: 'searchInput',
      desc: '',
      args: [],
    );
  }

  /// `Total tax`
  String get totalTax {
    return Intl.message(
      'Total tax',
      name: 'totalTax',
      desc: '',
      args: [],
    );
  }

  /// `Invalid SMS Verification code`
  String get invalidSMSCode {
    return Intl.message(
      'Invalid SMS Verification code',
      name: 'invalidSMSCode',
      desc: '',
      args: [],
    );
  }

  /// `Get code`
  String get sendSMSCode {
    return Intl.message(
      'Get code',
      name: 'sendSMSCode',
      desc: '',
      args: [],
    );
  }

  /// `Verify`
  String get verifySMSCode {
    return Intl.message(
      'Verify',
      name: 'verifySMSCode',
      desc: '',
      args: [],
    );
  }

  /// `Show Gallery`
  String get showGallery {
    return Intl.message(
      'Show Gallery',
      name: 'showGallery',
      desc: '',
      args: [],
    );
  }

  /// `Discount`
  String get discount {
    return Intl.message(
      'Discount',
      name: 'discount',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get username {
    return Intl.message(
      'Username',
      name: 'username',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Enter your email`
  String get enterYourEmail {
    return Intl.message(
      'Enter your email',
      name: 'enterYourEmail',
      desc: '',
      args: [],
    );
  }

  /// `Enter your password`
  String get enterYourPassword {
    return Intl.message(
      'Enter your password',
      name: 'enterYourPassword',
      desc: '',
      args: [],
    );
  }

  /// `I want to create an account`
  String get iwantToCreateAccount {
    return Intl.message(
      'I want to create an account',
      name: 'iwantToCreateAccount',
      desc: '',
      args: [],
    );
  }

  /// `Login to your account`
  String get loginToYourAccount {
    return Intl.message(
      'Login to your account',
      name: 'loginToYourAccount',
      desc: '',
      args: [],
    );
  }

  /// `Create an account`
  String get createAnAccount {
    return Intl.message(
      'Create an account',
      name: 'createAnAccount',
      desc: '',
      args: [],
    );
  }

  /// `Coupon code`
  String get couponCode {
    return Intl.message(
      'Coupon code',
      name: 'couponCode',
      desc: '',
      args: [],
    );
  }

  /// `Remove`
  String get remove {
    return Intl.message(
      'Remove',
      name: 'remove',
      desc: '',
      args: [],
    );
  }

  /// `Congratulations! Coupon code applied successfully`
  String get couponMsgSuccess {
    return Intl.message(
      'Congratulations! Coupon code applied successfully',
      name: 'couponMsgSuccess',
      desc: '',
      args: [],
    );
  }

  /// `Your address is exist in your local`
  String get saveAddressSuccess {
    return Intl.message(
      'Your address is exist in your local',
      name: 'saveAddressSuccess',
      desc: '',
      args: [],
    );
  }

  /// `Your note`
  String get yourNote {
    return Intl.message(
      'Your note',
      name: 'yourNote',
      desc: '',
      args: [],
    );
  }

  /// `Write your note`
  String get writeYourNote {
    return Intl.message(
      'Write your note',
      name: 'writeYourNote',
      desc: '',
      args: [],
    );
  }

  /// `You've successfully placed the order`
  String get orderSuccessTitle1 {
    return Intl.message(
      'You\'ve successfully placed the order',
      name: 'orderSuccessTitle1',
      desc: '',
      args: [],
    );
  }

  /// `You can check status of your order by using our delivery status feature. You will receive an order confirmation e-mail with details of your order and a link to track its progress.`
  String get orderSuccessMsg1 {
    return Intl.message(
      'You can check status of your order by using our delivery status feature. You will receive an order confirmation e-mail with details of your order and a link to track its progress.',
      name: 'orderSuccessMsg1',
      desc: '',
      args: [],
    );
  }

  /// `Your account`
  String get orderSuccessTitle2 {
    return Intl.message(
      'Your account',
      name: 'orderSuccessTitle2',
      desc: '',
      args: [],
    );
  }

  /// `You can log to your account using e-mail and password defined earlier. On your account you can edit your profile data, check history of transactions, edit subscription to newsletter.`
  String get orderSuccessMsg2 {
    return Intl.message(
      'You can log to your account using e-mail and password defined earlier. On your account you can edit your profile data, check history of transactions, edit subscription to newsletter.',
      name: 'orderSuccessMsg2',
      desc: '',
      args: [],
    );
  }

  /// `Sign In`
  String get signIn {
    return Intl.message(
      'Sign In',
      name: 'signIn',
      desc: '',
      args: [],
    );
  }

  /// `Sign Up`
  String get signUp {
    return Intl.message(
      'Sign Up',
      name: 'signUp',
      desc: '',
      args: [],
    );
  }

  /// `Next`
  String get next {
    return Intl.message(
      'Next',
      name: 'next',
      desc: '',
      args: [],
    );
  }

  /// `Done`
  String get done {
    return Intl.message(
      'Done',
      name: 'done',
      desc: '',
      args: [],
    );
  }

  /// `Currencies`
  String get currencies {
    return Intl.message(
      'Currencies',
      name: 'currencies',
      desc: '',
      args: [],
    );
  }

  /// `Sale {percent} %`
  String sale(Object percent) {
    return Intl.message(
      'Sale $percent %',
      name: 'sale',
      desc: '',
      args: [percent],
    );
  }

  /// `Update Profile`
  String get updateUserInfor {
    return Intl.message(
      'Update Profile',
      name: 'updateUserInfor',
      desc: '',
      args: [],
    );
  }

  /// `Update`
  String get update {
    return Intl.message(
      'Update',
      name: 'update',
      desc: '',
      args: [],
    );
  }

  /// `About Us`
  String get aboutUs {
    return Intl.message(
      'About Us',
      name: 'aboutUs',
      desc: '',
      args: [],
    );
  }

  /// `Display name`
  String get displayName {
    return Intl.message(
      'Display name',
      name: 'displayName',
      desc: '',
      args: [],
    );
  }

  /// `Nice name`
  String get niceName {
    return Intl.message(
      'Nice name',
      name: 'niceName',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get english {
    return Intl.message(
      'English',
      name: 'english',
      desc: '',
      args: [],
    );
  }

  /// `Vietnam`
  String get vietnamese {
    return Intl.message(
      'Vietnam',
      name: 'vietnamese',
      desc: '',
      args: [],
    );
  }

  /// `Arabic`
  String get arabic {
    return Intl.message(
      'Arabic',
      name: 'arabic',
      desc: '',
      args: [],
    );
  }

  /// `Spanish`
  String get spanish {
    return Intl.message(
      'Spanish',
      name: 'spanish',
      desc: '',
      args: [],
    );
  }

  /// `Chinese`
  String get chinese {
    return Intl.message(
      'Chinese',
      name: 'chinese',
      desc: '',
      args: [],
    );
  }

  /// `Japanese`
  String get japanese {
    return Intl.message(
      'Japanese',
      name: 'japanese',
      desc: '',
      args: [],
    );
  }

  /// `The Language is updated successfully`
  String get languageSuccess {
    return Intl.message(
      'The Language is updated successfully',
      name: 'languageSuccess',
      desc: '',
      args: [],
    );
  }

  /// `Privacy and Term`
  String get agreeWithPrivacy {
    return Intl.message(
      'Privacy and Term',
      name: 'agreeWithPrivacy',
      desc: '',
      args: [],
    );
  }

  /// `Privacy and Term`
  String get PrivacyAndTerm {
    return Intl.message(
      'Privacy and Term',
      name: 'PrivacyAndTerm',
      desc: '',
      args: [],
    );
  }

  /// `I agree with`
  String get iAgree {
    return Intl.message(
      'I agree with',
      name: 'iAgree',
      desc: '',
      args: [],
    );
  }

  /// `Current Password`
  String get currentPassword {
    return Intl.message(
      'Current Password',
      name: 'currentPassword',
      desc: '',
      args: [],
    );
  }

  /// `New Password`
  String get newPassword {
    return Intl.message(
      'New Password',
      name: 'newPassword',
      desc: '',
      args: [],
    );
  }

  /// `have been added to your cart`
  String get addToCartSucessfully {
    return Intl.message(
      'have been added to your cart',
      name: 'addToCartSucessfully',
      desc: '',
      args: [],
    );
  }

  /// `Pull to Load more`
  String get pullToLoadMore {
    return Intl.message(
      'Pull to Load more',
      name: 'pullToLoadMore',
      desc: '',
      args: [],
    );
  }

  /// `Load Failed!`
  String get loadFail {
    return Intl.message(
      'Load Failed!',
      name: 'loadFail',
      desc: '',
      args: [],
    );
  }

  /// `Release to load more`
  String get releaseToLoadMore {
    return Intl.message(
      'Release to load more',
      name: 'releaseToLoadMore',
      desc: '',
      args: [],
    );
  }

  /// `No more Data`
  String get noData {
    return Intl.message(
      'No more Data',
      name: 'noData',
      desc: '',
      args: [],
    );
  }

  /// `All`
  String get all {
    return Intl.message(
      'All',
      name: 'all',
      desc: '',
      args: [],
    );
  }

  /// `Filter`
  String get filter {
    return Intl.message(
      'Filter',
      name: 'filter',
      desc: '',
      args: [],
    );
  }

  /// `Tags`
  String get tags {
    return Intl.message(
      'Tags',
      name: 'tags',
      desc: '',
      args: [],
    );
  }

  /// `Categories`
  String get categories {
    return Intl.message(
      'Categories',
      name: 'categories',
      desc: '',
      args: [],
    );
  }

  /// `Attributes`
  String get attributes {
    return Intl.message(
      'Attributes',
      name: 'attributes',
      desc: '',
      args: [],
    );
  }

  /// `Total order value must be at least `
  String get minCartValue {
    return Intl.message(
      'Total order value must be at least ',
      name: 'minCartValue',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ar'),
      Locale.fromSubtags(languageCode: 'es'),
      Locale.fromSubtags(languageCode: 'ja'),
      Locale.fromSubtags(languageCode: 'vi'),
      Locale.fromSubtags(languageCode: 'zh'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}