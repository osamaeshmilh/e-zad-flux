import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config/general.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/user.dart' as s;

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _auth = FirebaseAuth.instance;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String firstName, lastName, username, password;
  final TextEditingController _usernameController = TextEditingController();
  bool isChecked = false;

  void _welcomeDiaLog(s.User user) {
    var email = user.email;
    _snackBar('Welcome $email!');
    if (kLayoutWeb) {
      Navigator.of(context).pushReplacementNamed('/home-screen');
    } else {
      Navigator.of(context).pushReplacementNamed('/home');
    }
  }

  void _failMess(message) {
    _snackBar(message);
  }

  void _snackBar(String text) {
    final snackBar = SnackBar(
      content: Text('$text'),
      duration: Duration(seconds: 10),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    _submitRegister(firstName, lastName, username, password) {
      if (firstName == null ||
          lastName == null ||
          username == null ||
          password == null) {
        _snackBar('Please input fill in all fields');
      } else if (isChecked == false) {
        _snackBar('Please agree with our terms');
      } else {
        Provider.of<s.UserModel>(context, listen: false).createUser(
          username: username,
          password: password,
          firstName: firstName,
          lastName: lastName,
          success: _welcomeDiaLog,
          fail: _failMess,
        );
      }
    }

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            } else {
              Navigator.of(context).pushNamed('/home');
            }
          },
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0.0,
      ),
      body: SafeArea(
        child: InkWell(
          onTap: () => Utils.hideKeyboard(context),
          child: ListenableProvider.value(
            value: Provider.of<s.UserModel>(context),
            child: Consumer<s.UserModel>(
              builder: (context, value, child) {
                return SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        SizedBox(
                          height: 70.0,
                        ),
                        Container(
                          child: Center(
                            child: Image.asset(
                              kLogo,
                              width: MediaQuery.of(context).size.width / 2,
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        TextField(
                          onChanged: (value) => firstName = value,
                          decoration: InputDecoration(
                            labelText: S.of(context).firstName,
                          ),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        TextField(
                          onChanged: (value) => lastName = value,
                          decoration: InputDecoration(
                            labelText: S.of(context).lastName,
                          ),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        TextField(
                          controller: _usernameController,
                          onChanged: (value) => username = value,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                              labelText: S.of(context).enterYourEmail),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        TextField(
                          obscureText: true,
                          onChanged: (value) => password = value,
                          decoration: InputDecoration(
                            labelText: S.of(context).enterYourPassword,
                          ),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        Row(
                          children: <Widget>[
                            Checkbox(
                              value: isChecked,
                              activeColor: Theme.of(context).primaryColor,
                              checkColor: Colors.white,
                              onChanged: (value) {
                                isChecked = !isChecked;
                                setState(() {});
                              },
                            ),
                            Text(
                              S.of(context).iwantToCreateAccount,
                              style: TextStyle(fontSize: 16.0),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Checkbox(
                              value: isChecked,
                              activeColor: Theme.of(context).primaryColor,
                              checkColor: Colors.white,
                              onChanged: (value) {
                                isChecked = !isChecked;
                                setState(() {});
                              },
                            ),
                            Text(
                              S.of(context).iAgree,
                              style: TextStyle(fontSize: 16.0),
                            ),
                            SizedBox(width: 6),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PrivacyScreen()),
                                );
                              },
                              child: Text(
                                S.of(context).agreeWithPrivacy,
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 16.0,
                                    decoration: TextDecoration.underline),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Material(
                            color: Theme.of(context).primaryColor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                            elevation: 0,
                            child: MaterialButton(
                              onPressed: () async {
                                try {
                                  await _auth.createUserWithEmailAndPassword(
                                      email: username, password: password);
                                } catch (e) {
                                  printLog("[Resistration] ${e.toString()}");
                                }
                                _submitRegister(
                                    firstName, lastName, username, password);
                              },
                              minWidth: 200.0,
                              elevation: 0.0,
                              height: 42.0,
                              child: Text(
                                value.loading == true
                                    ? S.of(context).loading
                                    : S.of(context).createAnAccount,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                S.of(context).or + ' ',
                                style: TextStyle(color: Colors.black45),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Text(
                                  S.of(context).loginToYourAccount,
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      decoration: TextDecoration.underline),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

class PrivacyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).agreeWithPrivacy,
          style: TextStyle(
            fontSize: 16.0,
            color: Colors.white,
          ),
        ),
        leading: GestureDetector(
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onTap: () => Navigator.pop(context),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text('''
Under Vietnamese laws, users’ information such as names, email addresses, passwords and date of birth could be classified as “personal information. 


In particular,            
(a) Under Decree 72/2013, personal information is defined as information  which  is  attached  to  the  identification  of  the  identity  and personal  details  of  an  individual  including name,  age,  address,  people's  identity  card  number, telephone number, email address and other information as stipulated by law; 

(b) Under Circular 25/2010,  personal information means information sufficient to precisely identify an individual, which includes at least one of the following details: full name, birth date, occupation, title, contact address, email address, telephone number, identity card number and passport number. Information of personal privacy includes health record, tax payment record, social insurance card number, credit card number and other personal secrets.  
            
Circular 25 applies to the collection and use of personal information by websites operated by Vietnamese Government authorities. Circular 25 is not directly applicable to the collection and use of personal information by websites operated by non-Government entities. However, the provisions of Circular 25 could be applied by analogy. In addition, it is likely that a non-Government entity will be subject to the same or more stringent standards than those applicable to a Government entity;
            ''',
              style: TextStyle(fontSize: 16.0, height: 1.4),
              textAlign: TextAlign.justify),
        ),
      ),
    );
  }
}
