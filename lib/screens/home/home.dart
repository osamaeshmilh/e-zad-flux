import 'dart:async';
import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fstore/models/user.dart';
import 'package:fstore/widgets/home/search/custom_search.dart';
import 'package:geocoder/geocoder.dart';

import 'package:location/location.dart';
import 'package:package_info/package_info.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../common/constants.dart';
import '../../models/app.dart';
import '../../models/category.dart';
import '../../models/filter_attribute.dart';
import '../../models/filter_tags.dart';
import '../../widgets/home/background.dart';
import '../../widgets/home/index.dart';
import 'deeplink_item.dart';
import 'package:fstore/widgets/home/search/custom_search_page.dart' as search;
import 'package:http/http.dart' as h;

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen>
    with
        AfterLayoutMixin<HomeScreen>,
        AutomaticKeepAliveClientMixin<HomeScreen> {
  int city;

  @override
  bool get wantKeepAlive => true;

  LocationData currentLocation;
  String address;
  Uri _latestUri;
  StreamSubscription _sub;
  int itemId;

  static const APP_STORE_URL = 'https://apps.apple.com/us/app/alzad-store/id1510125786';
  static const PLAY_STORE_URL = 'https://play.google.com/store/apps/details?id=ly.com.ezad';

  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }

  @override
  void initState() {
    printLog("[Home] initState");

    initPlatformState();
    getCity().then((value) {
      setState(() {
        city = value;
      });
    });

    PackageInfo.fromPlatform().then((
        PackageInfo packageInfo) async {
      try {
        var response = await h.get(
            "http://app.elzad.ly/update.html");
        if (int.parse(packageInfo.buildNumber) <
            int.parse(response.body)) {
          _showVersionDialog(context);
        }
        print(response.body);
      } catch (e) {
        print(e);
      }
      print(packageInfo.buildNumber);
    });

    super.initState();
  }

  initPlatformState() async {
    await initPlatformStateForStringUniLinks();
  }

  initPlatformStateForStringUniLinks() async {
    // Attach a listener to the links stream
    _sub = getLinksStream().listen((String link) {
      if (!mounted) return;
      setState(() {
        _latestUri = null;
        try {
          if (link != null) _latestUri = Uri.parse(link);
          setState(() {
            itemId = int.parse(_latestUri.path.split('/')[1]);
          });

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ItemDeepLink(
                itemId: itemId,
              ),
            ),
          );
        } on FormatException {
          printLog('[initPlatformStateForStringUniLinks] error');
        }
      });
    }, onError: (err) {
      if (!mounted) return;
      setState(() {
        _latestUri = null;
      });
    });

    getLinksStream().listen((String link) {
      print('got link: $link');
    }, onError: (err) {
      print('got err: $err');
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    Provider.of<CategoryModel>(context, listen: false).getCategories(
        lang: Provider.of<AppModel>(context, listen: false).locale);
    Provider.of<FilterAttributeModel>(context, listen: false)
        .getFilterAttributes();
    Provider.of<FilterTagModel>(context, listen: false).getFilterTags();
    getCity().then((value) {
      setState(() {
        city = value;
      });
    });
  }

  Future<int> getCity() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.getInt("city");
  }

  Future<bool> setCity(cityId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setInt("city", cityId);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    printLog("[Home] build");

    return Scaffold(
      appBar: AppBar(
          title: GestureDetector(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return SimpleDialog(
                      title: Text("اختر المدينة"),
                      children: <Widget>[
                        SimpleDialogOption(
                          child: Text('مدينة طرابلس'),
                          onPressed: () {
                            setCity(1).then((value) {
                             //Provider.of<UserModel>(context, listen: false).logout();
                              Navigator.pop(context);
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: Text('تم تغيير المدينة'),
                                  content: Text('تم تغيير المدينة الى طرابلس, الرجاء اعادة تشغيل التطبيق !'),
                                  actions: <Widget>[
                                    FlatButton(
                                      onPressed: () => exit(0),
                                      child: Text('حسنا'),
                                    ),
                                  ],
                                ),
                              );
                            });
                          },
                        ),
                        SimpleDialogOption(
                          child: Text('مدينة بنغازي'),
                          onPressed: () {
                            setCity(2).then((value) {
                              //Provider.of<UserModel>(context, listen: false).logout();
                              Navigator.pop(context);
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: Text('تم تغيير المدينة'),
                                  content: Text('تم تغيير المدينة الى بنغازي, الرجاء اعادة تشغيل التطبيق !'),
                                  actions: <Widget>[
                                    FlatButton(
                                      onPressed: () => exit(0),
                                      child: Text('حسنا'),
                                    ),
                                  ],
                                ),
                              );
                            });
                          },
                        ),
                        SimpleDialogOption(
                          child: Text('مدينة مصراتة'),
                          onPressed: () {
                            setCity(3).then((value) {
                              //Provider.of<UserModel>(context, listen: false).logout();
                              Navigator.pop(context);
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: Text('تم تغيير المدينة'),
                                  content: Text('تم تغيير المدينة الى مصراتة, الرجاء اعادة تشغيل التطبيق !'),
                                  actions: <Widget>[
                                    FlatButton(
                                      onPressed: () => exit(0),
                                      child: Text('حسنا'),
                                    ),
                                  ],
                                ),
                              );
                            });
                          },
                        )
                      ],
                    );
                  });
            },
            child: Center(
                child: Container(
                  margin: const EdgeInsets.all(15.0),
                  padding: const EdgeInsets.all(3.0),
                  width: 150.0,
                  height: 30.0,
                  decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(color: Colors.white),
              ),
                  child: city == 2
                  ? Text(
                    "مدينة بنغازي",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                    ) : city == 3 ? Text(
                      "مدينة مصراتة",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                    ) : Text(
                    "مدينة طرابلس",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
            )),
          ),
          leading: IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/search');
              }),
          actions: <Widget>[
            Container(
              margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.all(3.0),
              child: Image.network(
                "https://app.elzad.ly/wp-content/uploads/2020/05/E-zad.png",
              ),
            ),
          ]),
      body: SafeArea(
        child: Consumer<AppModel>(
          builder: (context, value, child) {
            if (value.appConfig == null) {
              return kLoadingWidget(context);
            }
            return Stack(children: <Widget>[
              if (value.appConfig['Background'] != null)
                HomeBackground(config: value.appConfig['Background']),
              HomeLayout(
                configs: value.appConfig,
                key: Key(value.locale),
              ),
            ]);
          },
        ),
      ),
    );
  }

  _getUserLocation() async {
    //call this async method from whereever you need

    LocationData myLocation;
    String error;
    Location location = new Location();
    try {
      myLocation = await location.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'please grant permission';
        print(error);
      }
      if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'permission denied- please enable it from app settings';
        print(error);
      }
      myLocation = null;
    }
    currentLocation = myLocation;
    final coordinates = Coordinates(myLocation.latitude, myLocation.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    setState(() {
      address = '${first.addressLine}';
    });
    //address = '${first.addressLine}';
    // return address;
  }


  _showVersionDialog(context) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "تحديث للتطبيق";
        String message = "يوجد تحديث جديد للتطبيق متاح عبر المتجر الان, قم بتحميله!";
        String btnLabel = "تحميل";
        return Platform.isIOS
            ? CupertinoAlertDialog(title: Text(title), content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(btnLabel),
              onPressed: () => launch(APP_STORE_URL),
            ),
          ],
        )
            : AlertDialog(title: Text(title), content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(btnLabel),
              onPressed: () => launch(PLAY_STORE_URL),
            ),
          ],
        );
      },
    );
  }

}
