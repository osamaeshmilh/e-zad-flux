import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/config.dart' as config;
import '../../common/constants.dart';

class OnBoardScreen extends StatefulWidget {
  @override
  _OnBoardScreenState createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> {
  final List<Slide> slides = [];
  final isRequiredLogin = config.kAdvanceConfig['IsRequiredLogin'];

  @override
  void initState() {
    Widget loginWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Image.asset(
          kOrderCompleted,
          height: 200,
          fit: BoxFit.fitWidth,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                child: Text(
                  ' طرابلس ',
                  style: TextStyle(color: kTeal400, fontSize: 20.0),
                ),
                onTap: () async {
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  await prefs.setInt("city", 1);
                  await Navigator.pushNamed(context, RouteList.home);
                },
              ),
              Text(
                "|",
                style: TextStyle(color: kTeal400, fontSize: 20.0),
              ),
              GestureDetector(
                child: Text(
                  ' بنغازي ',
                  style: TextStyle(color: kTeal400, fontSize: 20.0),
                ),
                onTap: () async {
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    await prefs.setInt("city", 2);
                    await Navigator.pushNamed(context, RouteList.home);
                },
              ),
              Text(
                "|",
                style: TextStyle(color: kTeal400, fontSize: 20.0),
              ),
              GestureDetector(
                child: Text(
                  ' مصراتة ',
                  style: TextStyle(color: kTeal400, fontSize: 20.0),
                ),
                onTap: () async {
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  await prefs.setInt("city", 3);
                  await Navigator.pushNamed(context, RouteList.home);
                },
              ),
            ],
          ),
        ),
      ],
    );

    for (int i = 0; i < config.onBoardingData.length; i++) {
      Slide slide = Slide(
        title: config.onBoardingData[i]['title'],
        description: config.onBoardingData[i]['desc'],
        marginTitle: EdgeInsets.only(
          top: 125.0,
          bottom: 50.0,
        ),
        maxLineTextDescription: 2,
        styleTitle: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 25.0,
          color: kGrey900,
        ),
        backgroundColor: Colors.white,
        marginDescription: EdgeInsets.fromLTRB(20.0, 75.0, 20.0, 0),
        styleDescription: TextStyle(
          fontSize: 15.0,
          color: kGrey600,
        ),
        foregroundImageFit: BoxFit.fitWidth,
      );

      if (i == 2) {
        slide.centerWidget = loginWidget;
      } else {
        slide.pathImage = config.onBoardingData[i]['image'];
      }

      slides.add(slide);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(
      slides: slides,
      styleNameSkipBtn: TextStyle(color: kGrey900),
      nameSkipBtn:'تخطي',
      styleNameDoneBtn: TextStyle(color: kGrey900),
      nameNextBtn: 'التالي',
      nameDoneBtn: isRequiredLogin ? '' : 'انهاء',
      onDonePress: () async {
        await Navigator.pushNamed(context, RouteList.home);
      },
    );
  }
}
