import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

import '../../common/config.dart';
import '../../common/constants.dart';

class PaymentWebview extends StatefulWidget {
  final String url;
  final Function onFinish;

  PaymentWebview({this.onFinish, this.url});

  @override
  State<StatefulWidget> createState() {
    return PaymentWebviewState();
  }
}

class PaymentWebviewState extends State<PaymentWebview> {
  @override
  void initState() {
    super.initState();
    final flutterWebviewPlugin = FlutterWebviewPlugin();
    flutterWebviewPlugin.onUrlChanged.listen((String url) {
      print("URL: " + url);
      if (url.startsWith("https://app.elzad.ly/checkout/order-received/")) {
        final items = url.split("/order-received/");
        if (items.length > 1) {
          final number = items[1].split("/")[0];
          widget.onFinish(number);
          Navigator.of(context).pop();
        }
      }
      if (url.contains("checkout/success")) {
        widget.onFinish("0");
        Navigator.of(context).pop();
      }
    });
    flutterWebviewPlugin.onProgressChanged.listen((event) {
      if(event < 1.0){
        Container(child: kLoadingWidget(context));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var checkoutUrl = "";

    var headers = Map<String, String>();
    if (serverConfig["type"] == "woo") {
      checkoutUrl = widget.url;
    }

    return WebviewScaffold(
      withJavascript: true,
      appCacheEnabled: true,
      url: checkoutUrl,
      headers: headers,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        backgroundColor: kGrey200,
        elevation: 0.0,
      ),
      withZoom: true,
      withLocalStorage: true,
      hidden: true,
      initialChild: Container(child: kLoadingWidget(context)),
    );
//    return Scaffold(
//      appBar: AppBar(
//        leading: IconButton(
//            icon: Icon(Icons.arrow_back),
//            onPressed: () {
//              Navigator.of(context).pop();
//            }),
//        backgroundColor: kGrey200,
//        elevation: 0.0,
//      ),
//      body: WebView(
//          javascriptMode: JavascriptMode.unrestricted,
//          initialUrl: checkoutUrl,
//          onPageFinished: (String url) {
//            if(url.contains("/checkout/order-received/")){
//              final items = url.split("/checkout/order-received/");
//              if(items.length > 1){
//                final number = items[1].split("/")[0];
//                onFinish(number);
//                Navigator.of(context).pop();
//              }
//            }
//          }),
//    );
  }
}
