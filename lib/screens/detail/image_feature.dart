import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/tools.dart';
import '../../models/product.dart';
import '../../widgets/common/image_galery.dart';

class ImageFeature extends StatelessWidget {
  final Product product;

  ImageFeature(this.product);

  @override
  Widget build(BuildContext context) {
    ProductVariation productVariation;
    productVariation = Provider.of<ProductModel>(context).productVariation;
    final imageFeature = productVariation != null
        ? productVariation.imageFeature
        : product.imageFeature;

    _onShowGallery(context, [index = 0]) {
      Navigator.push(context, PageRouteBuilder(pageBuilder: (context, __, ___) {
        return ImageGalery(images: product.images, index: index);
      }));
    }

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return FlexibleSpaceBar(
          background: GestureDetector(
              onTap: () => _onShowGallery(context),
              child: Container(
                child: kProductDetail['isHero']
                    ?Center(
                    child:Hero(
                   tag: 'product-${product.id}',
                       child:  Image.network(
                         imageFeature,
                         fit: BoxFit.contain,
                       ),
                ),
                )
                                : Center(
                 child: Image.network(
                   imageFeature,
                   fit: BoxFit.contain,
                 ),
                ),
              )),
        );
      },
    );
  }
}
