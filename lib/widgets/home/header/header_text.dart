import 'package:flutter/material.dart';

import '../../../common/tools.dart';
import '../../../widgets/home/search/custom_search.dart';
import '../../../widgets/home/search/custom_search_page.dart' as search;
import 'header_type.dart';

class HeaderText extends StatelessWidget {
  final config;

  HeaderText({this.config, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: config['height'] != null ? MediaQuery.of(context).size.height * config['height'] : 60,
      padding: EdgeInsets.only(
          //top: config['white'] == true ? 5.0 : 15.0,
          left: Tools.formatDouble(config['padding'] ?? 20.0),
          right: Tools.formatDouble(config['padding'] ?? 20.0),
          //bottom: config['white'] == true ? 60.0 : 10.0
         ),
      width: MediaQuery.of(context).size.width,
      child: SafeArea(
        bottom: false,
        top: config['isSafeArea'] == true,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            HeaderType(config: config),

          ],
        ),
      ),
    );
  }
}
