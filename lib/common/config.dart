export 'config/advertise.dart';
export 'config/general.dart';
export 'config/onboarding.dart';
export 'config/payments.dart';
export 'config/products.dart';
export 'config/smartchat.dart';

/// Server config demo for WooCommerce
const serverConfig = {
  "type": "woo", // use mock data, change to: type "woo-mock" and empty other values
  "url": "https://app.elzad.ly",
  "url2": "https://bn.elzad.ly",
  "url3": "https://ms.elzad.ly",
  "consumerKey": "ck_ee075d7130081bdfa1779f1c40450a494bb0c3a9",
  "consumerSecret": "cs_49ed27efe47c70a083d9e724fb248f528946a009",
  "blog": "https://app.elzad.ly", // Your website woocommerce. You can remove this line if it same url
  "forgetPassword": "https://app.elzad.ly/wp-login.php?action=lostpassword"
};
