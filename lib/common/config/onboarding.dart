/// the welcome screen data
List onBoardingData = [
  {
    "title": "مرحبا بكم في متجر الزاد",
    "image": "assets/images/fogg-delivery-1.png",
    "desc": "تسوق في مختلف المنتجات "
  },
  {
    "title": "توصيل مجاني",
    "image": "assets/images/fogg-uploading-1.png",
    "desc":
        "توصيل مجاني للبضاعة الي منزلك"
  },
  {
    "title": "ابدأ الان!",
    "image": "fogg-order-completed.png",
    "desc": "قم بالتسجيل وتصفح منتجاتنا"
  },
];
