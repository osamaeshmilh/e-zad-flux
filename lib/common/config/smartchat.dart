import 'package:font_awesome_flutter/font_awesome_flutter.dart';

const kConfigChat = {
  "EnableSmartChat": true,
};

/// config for the chat app
const smartChat = [
  {
    'app': 'whatsapp://send?phone=00218910030860',
    'iconData': FontAwesomeIcons.whatsapp
  },
  {'app': 'tel:00218910030860', 'iconData': FontAwesomeIcons.phone},
  {'app': 'sms://00218910030860', 'iconData': FontAwesomeIcons.sms},
  {
    'app': 'https://www.facebook.com/EzadCommerce.ly/',
    'iconData': FontAwesomeIcons.facebookMessenger
  }
];
const String adminEmail = "sales@elzad.ly";
