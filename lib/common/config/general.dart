import '../../common/constants.dart';

/// Default app config
const kAppConfig = 'lib/config/config_en.json';

/// This option is determine hide some components for web
var kLayoutWeb = true;

/// The Google API Key to support Pick up the Address automatically
/// We recommend to generate both ios and android to restrict by bundle app id
/// The download package is remove these keys, please use your own key
const kGoogleAPIKey = {
  "android": "your-google-api-key",
  "ios": "your-google-api-key",
  "web": "your-google-api-key"
};

/// user for upgrader version of app, remove the comment from lib/app.dart to enable this feature
/// https://tppr.me/5PLpD
const kUpgradeURLConfig = {
  "android":
      "https://play.google.com/store/apps/details?id=app.elzad.ly",
  "ios": "https://apps.apple.com/us/app/"
};

/// use for rating app on store feature
const kStoreIdentifier = {
  "android": "ly.com.ezad",
  "ios": "1469772800"
};

const kAdvanceConfig = {
  "DefaultLanguage": "ar",
  "IsRequiredLogin": false,
  "DetailedBlogLayout": kBlogLayout.halfSizeImageType,

  "EnablePointReward": true,
  "EnableRating": true,
  "hideOutOfStock": true,
  "isCaching": true,

  "DefaultStoreViewCode": "", //for magento
  "EnableAttributesConfigurableProduct": ["color", "size"], //for magento
  "EnableAttributesLabelConfigurableProduct": ["color", "size"], //for magento,

  "GridCount": 3,

  "DefaultCurrency": {
    "symbol": "د.ل",
    "decimalDigits": 2,
    "symbolBeforeTheNumber": false,
    "currency": "LYD"
  },
  "Currencies": [
    {
      "symbol": "\$",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": true,
      "currency": "USD"
    },
    {
      "symbol": "د.ل",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": false,
      "currency": "LYD"
    }
  ],
};
